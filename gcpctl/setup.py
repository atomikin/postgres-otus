import setuptools
import pathlib
import glob
import os

print(glob.glob(str(pathlib.Path(__file__).parent.joinpath('gcpctl', 'templates/*.json.j2'))))

setuptools.setup(
    name='gcpctl',
    version=os.environ['VERSION'],
    install_requires=[
        'google-api-python-client==1.12.5',
        'pyyaml',
        'jinja2',
        'ansible',
    ],

    author='atomikin',
    author_email='atomikin@yandex.ru',
    packages=setuptools.find_packages(),
    python_requires='>=3.8',
    entry_points={
        'console_scripts': [
            'gcpctl=gcpctl.app:main',
        ]
    },
    include_package_data=True, 
    # package_data={
    #     'templates': glob.glob(str(pathlib.Path(__file__).parent.joinpath('gcpctl', 'templates/*.json.j2'))),
    # },
    # data_files=(
    #     (
    #         'gcpctl/templates', 
    #         glob.glob(str(pathlib.Path(__file__).parent.joinpath('templates')) + '/*.json.j2')
    #     ),
    # )
)
