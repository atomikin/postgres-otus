#!/usr/bin/env python3
import collections
import shlex
import pathlib
import argparse
import yaml
import os
import subprocess
import itertools
import sys
from typing import List

from .data import Instance, Config, Env, PosOpt
from .client import Client


PREPARATION_SCRIPT= '''
#!/usr/bin/env zsh

this_dir=$(cd $(dirname $0) && pwd)
ansible_dir="${this_dir}/ansible"

ansible-playbook -i "${this_dir}/inventory.yaml" "${ansible_dir}/playbook.yaml" $@
'''


def action_up(args):
    env: Env = args.prepare_env(args)
    p = env.load_project()
    if env.instances_json.is_file() and env.instances_json.stat().st_size != 0:
        print('Project already initialized...')
        return
    client = Client(p.name, p.zone)
    instances = collections.defaultdict(list)
    for instance in client.create(*Config.load(env.instances_config)):
        instances[instance.group].append(instance)
        # print(f'Instance {instance.name} info: ', instance)

    env.write_intances_json(
        [i._asdict() for i in itertools.chain(*instances.values())]
    )
        
def action_prepare_env(args):
    return Env.create_at(args.target)


def action_stop(args):
    env = Env.create_at(args.target)
    p = env.load_project()
    client = Client(p.name, p.zone)
    for instance in env.read_instances_json():
        client.stop(instance['name'])


def action_list_hosts(args):
    env = Env.create_at(args.target)
    # project = env.load_project()
    if not env.instances_json.exists():
        return
    print(*(f'{i.name}: {i.ip}' for i in env.load_instances()))


def action_ssh(args):
    # import pdb; pdb.set_trace()
    env = Env.create_at(args.target)
    instances = [i for i in env.load_instances() if i.name == args.host_alias]
    if not instances:
        raise ValueError(f'instance {args.host_alias} not found')
    instance: Instance = instances[0]
    inventory = env.load_inventory()
    group_vars = inventory[instance.group]['vars']
    cmd = shlex.split(
        f'ssh {group_vars["ansible_user"]}@{instance.ip} '
        f'{group_vars["ansible_ssh_common_args"]} '
        f'-i {group_vars["ansible_ssh_private_key_file"]} '
        + (args.cmd or '')
    )
    print('Running: ', *cmd)
    subprocess.run(cmd)


def action_start(args):
    env = Env.create_at(args.target)
    p = env.load_project()
    client = Client(p.name, p.zone)
    instances: List[Instance] = []
    for instance in env.read_instances_json():
        client.start(instance['name'])
        instances.append(
            client.prepare_instance(
                instance['group'], 
                instance['name'])._asdict()
        )
    env.write_intances_json(instances)
    if not env.inventory_file.exists():
        return
    data = env.load_inventory()
    for i in instances:
        hosts = data[i['group']]['hosts']
        if i['name'] in hosts:
            hosts[i['name']] = dict(ansible_host=i['ip'])
    env.dump_inventory(data)


def action_init(args):
    env = Env.create_at(args.target)
    env.dump_project(name=args.project, zone=args.zone)
    env.instances_config.touch()
    if not env.ansiblize_script.exists() or env.ansiblize_script.stat().st_size == 0:
        with env.ansiblize_script.open('w') as f:
            f.write(PREPARATION_SCRIPT)
        env.ansiblize_script.chmod(0o755)
    env.playbook.touch()
    env.instances_json.touch()
    if not env.roles_link.exists():
        # common = this_dir().joinpath('ansible', 'roles')
        common: pathlib.Path = args.ansible_roles_folder
        common = common.absolute().resolve()
        if not common.exists or common.name != 'roles':
            return
        rel_path = os.path.relpath(common, env.roles_link.parent)
        os.symlink(rel_path, env.roles_link)


def action_drop(args):
    env = Env.create_at(args.target)
    p = env.load_project()
    client = Client(p.name, p.zone)
    for instance in env.read_instances_json():
        client.delete(instance['name'])
    env.drop_env()

def action_create_inventory(args):
    env = Env.create_at(args.target)
    result = collections.defaultdict(dict)
    common_data = {
        'ansible_ssh_private_key_file': args.ansible_ssh_private_key_file,
        'ansible_user': args.ansible_user,
        'ansible_ssh_common_args': '-o StrictHostKeyChecking=no',
    }

    for instance in env.load_instances():
        if 'hosts' not in result[instance.group]:
            result[instance.group]['hosts'] = {}
        result[instance.group]['hosts'].update(**{instance.name: {'ansible_host': instance.ip}})
    for group in result:
        result[group]['vars'] = common_data
    
    env.dump_inventory(dict(result))
    if not env.playbook.exists() or env.playbook.stat().st_size == 0:
        dummy = []
        for i in result:
            dummy.append(
                {
                    'hosts': i,
                    'become': True,
                    'become_user': 'root',
                    'roles': ['base']
                }
            )
        with env.playbook.open('w') as f:
            yaml.safe_dump(dummy, f, sort_keys=False)


def main():
    parser = argparse.ArgumentParser('Prepare env')
    subs = parser.add_subparsers()
    up_parser = subs.add_parser('up')
    stop_parser = subs.add_parser('stop')
    start_parser = subs.add_parser('start')
    drop_parser = subs.add_parser('drop')
    init_parser = subs.add_parser('init')
    create_inventory = subs.add_parser('create-inventory')
    list_hosts = subs.add_parser('list-hosts')
    ssh = subs.add_parser('ssh')

    common_opts = [
        PosOpt(
            'target', type=pathlib.Path,
            help='Directory with target project',
            default=pathlib.Path.cwd(),
            nargs='?'
        ),
    ]
    init_parser.add_argument(
        '-p', '--project', required=True, type=str,
        help='Name of the roject on GCP'
    )
    init_parser.add_argument(
        '-z', '--zone', default='us-central1-a', type=str,
        help='GCP Zone'
    )
    init_parser.add_argument(
        '-r', '--ansible-roles-folder', default=pathlib.Path.cwd().joinpath('ansible', 'roles'),
        help='Folder with common set of ansible roles', type=pathlib.Path
    )

    create_inventory.add_argument(
        '-u', '--ansible-user', required=True, help='Ansible user'
    ) 
    create_inventory.add_argument(
        '-k', '--ansible-ssh-private-key-file', required=True,
        help='private key to be used'
    )
    
    parsers_with_common_args = (
        up_parser, stop_parser, start_parser,
        drop_parser, init_parser, create_inventory,
        list_hosts, ssh
    )
    for p in parsers_with_common_args:
        for opt in common_opts:
                opt.add_to_parser(p)

    up_parser.set_defaults(func=action_up)
    stop_parser.set_defaults(func=action_stop)
    start_parser.set_defaults(func=action_start)
    drop_parser.set_defaults(func=action_drop)
    init_parser.set_defaults(func=action_init)
    create_inventory.set_defaults(func=action_create_inventory)
    list_hosts.set_defaults(func=action_list_hosts)
    ssh.add_argument('host_alias', help='Host name to connect to')
    ssh.add_argument('-c', '--cmd', nargs='?')
    ssh.set_defaults(func=action_ssh)

    parser.set_defaults(prepare_env=action_prepare_env)
    args = parser.parse_args()
    try:
        args.func(args)
    except Exception as err:
        print('Unexpected error: ', str(err), file=sys.stderr)
    except KeyboardInterrupt as err:
        print('Keyboard Interrupt: ', str(err), file=sys.stderr)


if __name__ == '__main__':
    main()
