
from typing import Generator, Sequence, Dict, Any
import time
import pathlib
import json
import sys

import googleapiclient.discovery
import jinja2

from .data import Instance, Config

class Client:

    def __init__(self, project, zone):
        """
        Later on there there will be zone, project and so on.
        """
        self.project = project
        self.zone = zone
        self._compute = googleapiclient.discovery.build('compute', 'v1')
        # googleapiclient.discovery.build_from_document

    def create(self, *config: Sequence[Config]) -> Generator[Instance, None, None]:
        all_instances = []
        pipe = self._compute
        for cfg in config:
            all_instances.extend((cfg.group, i) for i in self.prepare_instances_config(cfg))
        for group, i in sorted(all_instances, key=lambda i: i[0]):
            req = self._compute.instances().insert(body=i, project=self.project, zone=self.zone)
            op = req.execute()
            if self.wait(op['name']):
                print(f'SUCCESS: {i["name"]} is up')
                yield self.prepare_instance(group, i['name'])
            else:
                raise RuntimeError(f'ERROR: failed to create {i["name"]}')

    def get(self, instance):
        return self._compute.instances().get(
            project=self.project, zone=self.zone, instance=instance
        ).execute()

    def stop(self, instance):
        op = self._compute.instances().stop(
            project=self.project,
            zone=self.zone,
            instance=instance
        ).execute()
        # import pdb; pdb.set_trace()
        if self.wait(op['name']):
            print(f'SUCCESS: {instance} stopped')
        else:
            print(f'ERROR: failed to stop {instance} instance', file=sys.stderr)


    def start(self, instance):
        op = self._compute.instances().start(
            project=self.project,
            zone=self.zone,
            instance=instance
        ).execute()
        # import pdb; pdb.set_trace()
        if self.wait(op['name']):
            print(f'SUCCESS: {instance} started')
        else:
            print(f'ERROR: failed to start {instance} instance', file=sys.stderr)     

    def delete(self, instance):
        op = self._compute.instances().delete(
            project=self.project,
            zone=self.zone,
            instance=instance
        ).execute()
        # import pdb; pdb.set_trace()
        if self.wait(op['name']):
            print(f'SUCCESS: {instance} deleted')
        else:
            print(f'ERROR: failed to deleted {instance} instance', file=sys.stderr)   

    def describe(self, base_config, name):
        pass

    def wait(self, operation, timeout_sec=60):
        start = time.time()
        while time.time() - start < timeout_sec:
            res = self._compute.zoneOperations().get(
                project=self.project,
                zone=self.zone,
                operation=operation
            ).execute()
            if res['status'] == 'DONE':
                msg = 'Operation completed'
                if 'error' in res:
                    print(f'{msg} with error: {res["error"]}', file=sys.stderr)
                    return False
                # print(f'{msg} successfully.')
                # pprint.pprint(res)
                return True
            time.sleep(1)
        print(f'Operation timeout ({timeout_sec})')
        return False

    def prepare_instances_config(self, config: Config) -> Sequence[Dict[str, Any]]:
        result = []
        with pathlib.Path(__file__).parent.joinpath('templates', config.template).open(encoding='utf-8') as f:
            template = jinja2.Template(f.read())
        for i in range(1, config.count + 1):
            params = dict(
                config._asdict(),
                name=f'{config.name}-{config.group}-{i}',
                project=self.project,
                zone=self.zone,
                region=self.zone.rsplit('-', 1)[0],
            )
            # print(template.render(**params))
            result.append(json.loads(template.render(**params)))
        return result

    def prepare_instance(self, group, instance) -> Instance:
        data = self.get(instance)
        ip = internal_ip = None
        for iface in data.get('networkInterfaces', ()):
            internal_ip = iface.get('networkIP', '')
            for cfg in iface.get('accessConfigs', ()):
                ip = cfg.get('natIP')
                # return Instance(instance, ip, group)
            if ip:
                break
        # import pdb; pdb.set_trace()
        return Instance(instance, ip, group, internal_ip)