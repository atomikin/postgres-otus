
import argparse
import json
import pathlib
import os
from typing import NamedTuple, Any, List, Dict

import yaml

class Config(NamedTuple):
    name: str
    disk_size: int = 10
    machine_type: str = 'e2-medium'
    count: int = 1
    template: str = 'base.json.j2'
    group: str = 'default'

    @classmethod
    def load(cls, path: str):
        path = pathlib.Path(path).absolute()
        # import pdb; pdb.set_trace()
        if not path.is_file():
            raise ValueError('Instances file does not exist')
        with path.open(encoding='utf-8') as f:
            return tuple(cls(**i) for i in yaml.load(f, Loader=yaml.FullLoader))


class Instance(NamedTuple):
    name: str
    ip: str
    group: str
    internal_ip: str = ''


class Opt(NamedTuple):
    short_name: str
    long_name: str
    default: Any = None
    required: bool = False
    type: Any = None
    help: str = None

    def add_to_parser(self, parser: argparse.ArgumentParser):
        parser.add_argument(
            self.short_name, self.long_name, default=self.default,
            required=self.required, type=self.type, help=self.help
        )


class PosOpt(NamedTuple):
    name: str
    type: Any = None
    help: str = None
    default: Any = None
    nargs: Any = None

    def add_to_parser(self, parser: argparse.ArgumentParser):
        parser.add_argument(
            self.name, type=self.type, help=self.help, default=self.default,
            nargs=self.nargs
        )


class Project(NamedTuple):
    name: str
    zone: str


class Env(NamedTuple):
    instances_config: pathlib.Path
    instances_json: pathlib.Path
    inventory_file: pathlib.Path
    project_json: pathlib.Path
    playbook: pathlib.Path
    roles_link: pathlib.Path
    ansiblize_script: pathlib.Path
    gcp_templates: pathlib.Path

    def write_intances_json(self, data: List[Dict[str, Any]]) -> None:
        with self.instances_json.open('w') as f:
            json.dump(data, f, indent=4)

    def read_instances_json(self):
        with self.instances_json.open() as f:
            return json.load(f)
    
    def write_inventory(self, data: Dict[str, List[Instance]]) -> None:
        pass

    def drop_env(self):
        if self.instances_json.exists():
            os.remove(self.instances_json)
        if self.inventory_file.exists():
            os.remove(self.inventory_file)
        if self.playbook.exists():
            os.remove(self.playbook)
        if self.roles_link.is_symlink():
            os.remove(self.roles_link)
            self.roles_link.parent.rmdir()
        if self.ansiblize_script.is_file():
            os.remove(self.ansiblize_script)
        if self.project_json.is_file():
            os.remove(self.project_json)

    def dump_project(self, **kwargs):
        with self.project_json.open('w') as f:
            json.dump(kwargs, f, indent=4)

    def load_project(self) -> Project:
        with self.project_json.open() as f:
            return Project(**json.load(f))

    def load_instances(self) -> List[Instance]:
        instances = []
        for i in self.read_instances_json():
            instances.append(
                Instance(group=i['group'], name=i['name'], ip=i['ip'])
            )
        return instances

    def dump_inventory(self, data: dict):
        with self.inventory_file.open('w') as f:
            yaml.safe_dump(data, f)
    
    def load_inventory(self):
        with self.inventory_file.open() as f:
            return yaml.safe_load(f)
            

    @classmethod
    def create_at(cls, home_dir: pathlib.Path):
        home_dir.mkdir(parents=True, mode=0o755, exist_ok=True)
        home_dir = home_dir.absolute().resolve()
        home_dir.joinpath('ansible').mkdir(mode=0o755, exist_ok=True)
        return cls(
            home_dir.joinpath('instances.yaml'),
            home_dir.joinpath('instances.json'),
            home_dir.joinpath('inventory.yaml'),
            home_dir.joinpath('project.json'),
            home_dir.joinpath('ansible', 'playbook.yaml'),
            home_dir.joinpath('ansible', 'roles'), 
            home_dir.joinpath('ansiblize'),
            home_dir.joinpath('templates'),
        )
