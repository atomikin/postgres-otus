Simple usage: 
```sh
$ gcpctl init homework/hw-1 --project gcp-project-name --zone some-zone \
  --ansible-roles-folder /some/path/ansible/roles
```
Where zone is a GCP defined region name for instance to be created at.
If ansible-roles-folder is provided, there will be a symlink to it at homework/hw-1/ansible.
It is useful when there is a set of common reusable roles.

fill in generated **homework/hw-1/intances.yaml** :
```yaml
- name: test
  disk_size: 40
  group: leader # name of the host group to be used in the ansible inventory file

- name: test
  group: follower
  count: 10
```
Here:
 * 
Redirect to homework/hw-1 and execute following:
```sh
$ gcpctl up
SUCCESS: test-leader-1 is up
SUCCESS: test-follower-1 is up
...
SUCCESS: test-follower-10 is up
$ gcpctl create-inventory -u username -k /path/to/private/key
```
After these 3 commands just run:
```sh
./homework/hw-1/ansiblize
```
To ssh to the instance:
```sh
gcpctl ssh test-leader-1
```
Alternatively one can just specify a command:
gcpctl ssh test-leader-1 -c "sudo -iu postgres pgbench -c8 -P 1 -T 500 -U postgres postgres"

Use following to stop the instance:
```ssh
gcpctl stop
```
It stops all the intances.

Folowing command removes the whole environment completely besides instances.yaml
```sh 
gcpctl drop
```

