#!/bin/bash
set -exu
exec migrate --source file://app/migrations --database "${PG_CONN_URL}" up