 # Задание: Нагрузочное тестирование и тюнинг PostgreSQL
 ### Цель: 
   * делать нагрузочное тестирование PostgreSQL
   * настраивать параметры PostgreSQL для достижения максимальной производительности
   ### Инструкция:
   * сделать проект <firstname>-<lastname>-<yyyymmdd>-10
   * сделать инстанс Google Cloud Engine типа e2-medium с ОС Ubuntu 20.04
   * поставить на него PostgreSQL 13 из пакетов собираемых postgres.org
   * настроить кластер PostgreSQL 13 на максимальную производительность не
      обращая внимание на возможные проблемы с надежностью в случае
      аварийной перезагрузки виртуальной машины
   * нагрузить кластер через утилиту
      https://github.com/Percona-Lab/sysbench-tpcc (требует установки
      https://github.com/akopytov/sysbench)
   
   ### Критерий выполнения: 
   * написать какого значения tps удалось достичь, показать какие параметры в
      какие значения устанавливали и почему
      Критерии оценки: Выполнение ДЗ: 1 из 2 частей - 5 баллов
      \+ 3 балла за задание со *
      \+ 1 балл за красивое решение
      \- 1 балл за рабочее решение, и недостатки указанные преподавателем не устранены
      **Рекомендуем сдать до: 09.12.2020**
# Результаты
#### Отклонения в сетапе:
1. взял **debian buster** (debian - огонь!!!1!), так как делаю все на нем. Виртуалки делаю через АПИ, образ не был шаблонизирован в конфиге :)
1. Проект я не стал пересоздавать, так как не выглядит принципиальным (поправьте если это не так)
#### Выполнение:
1. Настройка базового постгреса из репы postgresql.org делается ролью [potgres-server](../../ansible/roles/postgres-server/tasks/main.yml)
1. настройка sysbench - [sysbench-ready-host](../../ansible/roles/sysbench-ready-host), [скрипт-обертка](../../ansible/roles/sysbench-ready-host/templates/starter.sh.j2)
1. Прогон на дефортных настройках:
   1. TPS
   ![TPS](./pics/bench-6-3000-5-5-default-tps.png)
   1. QPS
   ![QPS](./pics/bench-6-3000-5-5-default-qps.png)
   1. LAT ![LAT](./pics/bench-6-3000-5-5-default-lat.png)
   1. Average:
   ```
        TPS: 37.68
        QPS: 1070.18
        LAT: 502.34
   ```
1. Требуется настроить несколько машин разным образом для параллельного заапуска  и сравнения. Некоторые идеи для тьюнинга:
   1. Использовать минимум соединений, то есть 6 (треды сисбенча) + 1
   1. shared_buffers - стоит увеличить, так как он изначально мал, а вроде стоит доводить до 1/4 оперативки. Оперативки 4 гига вроде.
   1. work_mem - нужно сравнить (ниже будет тест с увеличенным в 10 раз значением)
   1. autovacuum - говорят, что чаще - лучше
   1. autovacuum_work_mem - стоит сделать побольше
   1. synchronous_commit - off - сильно ускоряет процесс
   1. commit_delay - читал что помогает при большой нагрузке, когда в обработке от commit_siblings транзакций, будет вызвана задержка для их совместной фиксации. Раньше дела с этими настройками не имел :)
   1. wal_writer_delay = 500ms - Честно спер из примера в инете. видимо позволяет реже производить IO операции с диском, что хорошо, если ничего не упадет :)
1. [Общие опции](ansible/playbook.yaml#L16):
   ``` conf
      # autovacuum configs ...
      log_autovacuum_min_duration = 0
         autovacuum_max_workers = 10
         autovacuum_naptime = 15s
         autovacuum_vacuum_threshold = 25
         autovacuum_vacuum_scale_factor = 0.1
         autovacuum_vacuum_cost_delay = 10ms
         autovacuum_vacuum_cost_limit = 1000
      #
      # limit connections
         max_connections = 20 # 7 слишком мало для работы сисбенча :(
      # WAL
         synchronous_commit = off
         shared_buffers = 1024MB # 25% of RAM
         wal_buffers = -1
         commit_delay = 1000
         commit_siblings = 3
         wal_writer_delay = 500ms
   ```
   1. TPS 
   ![TPS](./pics/bench-6-3000-5-5-agrautovc-tps.png)
   1. QPS 
   ![QPS](./pics/bench-6-3000-5-5-agrautovc-qps.png)
   1. LAT
   ![LAT](./pics/bench-6-3000-5-5-agrautovc-lat.png)
   1. Average:
   ```
        TPS: 64.01
        QPS: 1821.59
        LAT: 621.40
   ```
1. [Опции](ansible/playbook.yaml#L51) в эффективности которых есть сомнения, добавляются только на группу машин `best`
   ```conf
   work_mem = 40MB # instead of 4MB
   maintanance_work_mem = 100MB # instead of 64MB
   ```
   1. TPS 
   ![TPS](./pics/bench-6-3000-5-5-best-tps.png)
   1. QPS 
   ![QPS](./pics/bench-6-3000-5-5-best-qps.png)
   1. LAT
   ![LAT](./pics/bench-6-3000-5-5-best-lat.png)
   1. Average:
   ```
        TPS: 65.13
        QPS: 1851.28
        LAT: 729.69
   ```
